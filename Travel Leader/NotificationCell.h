//
//  NotificationCell.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 3/27/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *datalb;
@property (weak, nonatomic) IBOutlet UIImageView *bellimg;
@property (weak, nonatomic) IBOutlet UILabel *titlelb;

@end
