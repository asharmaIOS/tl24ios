//
//  PreSignUpVC.h
//  Travel Leader
//
//  Created by wFares Dev on 7/8/19.
//  Copyright © 2019 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PreSignUpVC : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *suffixTxt;
@property (weak, nonatomic) IBOutlet UITextField *firstnameTxt;
@property (weak, nonatomic) IBOutlet UITextField *lastnameTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailtxt;
@property (weak, nonatomic) IBOutlet UITextField *recordLocatortxt;

@end

NS_ASSUME_NONNULL_END
