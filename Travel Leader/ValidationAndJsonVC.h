//
//  ValidationAndJsonVC.h
//  Travel Leader
//
//  Created by Abhishek Sharma on 1/31/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalViewController.h"

@interface ValidationAndJsonVC : UIViewController




+(NSDictionary *)GetParsingNewData:(NSDictionary *)ParsingDict GetUrl:(NSString *)urlStr GetTimeinterval:(NSTimeInterval)interval;
+(NSDictionary *)getParsingData:(NSDictionary *)ParsingDict GetUrl:(NSString *)urlStr GetTimeinterval:(NSTimeInterval)interval;


+(void)displayAlert:(NSString*)erroemessage HeaderMsg:(NSString*)HeaderMsg ;


@property (nonatomic, strong) NSString *serverCheckStr ;


@end
