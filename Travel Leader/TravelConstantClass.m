//
//  TravelConstantClass.m
//  Travel Leader
//
//  Created by Gurpreet Singh on 4/28/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import "TravelConstantClass.h"

@implementation TravelConstantClass


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.noGraphsAvailable = YES;
        //_testnameContainsValue =NO;
///_familyTest = NO;
        
        self.indexListarr = [[NSMutableArray alloc] init];
        self.TripArray = [[NSMutableArray alloc] init];
        self.sideseen = [[NSArray alloc] init];
        self.pnrlistdic = [[NSMutableDictionary alloc] init];
        self.cityarr = [[NSMutableArray alloc]init];
        self.imageURLarr = [[NSMutableArray alloc]init];    }
    return self;
}

+ (instancetype)singleton
{
    static TravelConstantClass *singletonObj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonObj = [[self alloc] init];
    });
    
    return singletonObj;
}





#pragma mark - web service common method



@end
