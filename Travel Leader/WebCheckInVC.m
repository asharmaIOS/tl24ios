//
//  WebCheckInVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 11/23/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//


#import <WebKit/WebKit.h>

#import "WebCheckInVC.h"

@interface WebCheckInVC () <WKNavigationDelegate, WKUIDelegate>


{
    UIView* loadingView;

    UIActivityIndicatorView *activityIndicatorObject;
   
}
@property(strong,nonatomic) WKWebView *webView;


@end

@implementation WebCheckInVC

- (void)viewDidLoad {
    
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                      initWithTitle:[_headerStr uppercaseString]
                                      style:UIBarButtonItemStylePlain
                                      target:self
                                    action:@selector(backButtonTapp:)];
      
      textButton1.tintColor = [UIColor whiteColor];
      self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
    
    NSLog(@"webaddress ===%@",_webLink);
    
    //self.productURL = @"http://www.URL YOU WANT TO VIEW GOES HERE";
    if (_webLink) {
        
        
        activityIndicatorObject = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
        activityIndicatorObject.hidden = NO;
        [activityIndicatorObject startAnimating];
        activityIndicatorObject.center = self.view.center;
       
        [self.view addSubview:activityIndicatorObject];
        
        
//        loadingView = [[UIView alloc]initWithFrame:CGRectMake(100, 400, 80, 80)];
//        loadingView.backgroundColor = [UIColor colorWithWhite:0. alpha:0.6];
//        loadingView.layer.cornerRadius = 5;
//
//        UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//        activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
//        [activityView startAnimating];
//        activityView.tag = 100;
//        [loadingView addSubview:activityView];
//
//        UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
//        lblLoading.text = @"Loading...";
//        lblLoading.textColor = [UIColor whiteColor];
//        lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:15];
//        lblLoading.textAlignment = NSTextAlignmentCenter;
//        [loadingView addSubview:lblLoading];
//
//        [self.view addSubview:loadingView];
        

       
    NSURL *url = [NSURL URLWithString:_webLink];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
        
    _webView = [[WKWebView alloc] initWithFrame:self.view.frame];
        _webView.backgroundColor = [UIColor clearColor];
    [_webView loadRequest:request];
        _webView.UIDelegate = self;
        self.webView.navigationDelegate = self;

    _webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:_webView];
    
    }
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    
    
    [activityIndicatorObject stopAnimating];
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    [activityIndicatorObject stopAnimating];
}
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
     [activityIndicatorObject startAnimating];
    
}

-(void)backButtonTapp:(id)sender {
    
   
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
