//
//  LeftDrawerVc.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 5/4/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftDrawerVc : UIViewController
- (IBAction)destinationbtn:(id)sender;
- (IBAction)logoutbtn:(id)sender;


@end
