//
//  CityCell.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 10/9/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *citylbl;

@end
