//
//  ReadMoreDescriptionOfdestinationViewController.h
//  Travel Leader
//
//  Created by Abhishek Sharma on 05/01/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadMoreDescriptionOfdestinationViewController : UIViewController <UIGestureRecognizerDelegate>


@property (weak, nonatomic) NSDictionary *dataDic;

@end
