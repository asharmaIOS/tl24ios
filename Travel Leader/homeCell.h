//
//  homeCell.h
//  Travel Leader
//
//  Created by Gurpreet Singh on 7/7/17.
//  Copyright © 2017 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface homeCell : UITableViewCell

 
@property (weak, nonatomic) IBOutlet UILabel *flightlbl;
@property (weak, nonatomic) IBOutlet UILabel *destinationlbl;
@property (weak, nonatomic) IBOutlet UIView *flightdateview;
@property (weak, nonatomic) IBOutlet UIView *destinationdateview;
@property (weak, nonatomic) IBOutlet UILabel *datelbl;
@property (weak, nonatomic) IBOutlet UILabel *monthlbl;
@property (weak, nonatomic) IBOutlet UILabel *yearlbl;
@property (weak, nonatomic) IBOutlet UILabel *datelb;
@property (weak, nonatomic) IBOutlet UILabel *monthlb;
@property (weak, nonatomic) IBOutlet UILabel *yearlb;




//*********Trip List
@property (weak, nonatomic) IBOutlet UILabel *flightnamelbl;
@property (weak, nonatomic) IBOutlet UILabel *fromcitylbl;
@property (weak, nonatomic) IBOutlet UILabel *tocitylbl;
@property (weak, nonatomic) IBOutlet UILabel *Airlinelbl;

@end
