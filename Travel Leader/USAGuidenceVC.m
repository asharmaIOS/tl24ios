//
//  USAGuidenceVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 15/07/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import "USAGuidenceVC.h"
#import "SVProgressHUD.h"
#import "WebCheckInVC.h"
#import "Reachability.h"
#import "ValidationAndJsonVC.h"
#import <QuartzCore/QuartzCore.h>




@interface USAGuidenceVC ()
{

NSArray *statedataArray;
}
@end

@implementation USAGuidenceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
       [self.navigationController.navigationBar setBarTintColor:[UIColor orangeColor]];
    
    [self setupLeftMenuButton];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
       NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
       
       if (networkStatus == NotReachable) {
           
           [ValidationAndJsonVC displayAlert:nil HeaderMsg:@"No internet connection found!"];

       }
    
       else {
    [self apiCallForUSA];
       }


}
-(void)backAction
{
    
    [self backButton:self];
}

-(void)setupLeftMenuButton
{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];

    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                    initWithTitle:@"USA GUIDANCE"
                                    style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];
}


-(void)backButton:(id)sender {

    [self.navigationController popViewControllerAnimated:NO];
 }


-(void)drawstateOfUSA :(NSArray *)dataArray {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        
       
        
    UIScrollView   * scrollViewForState = [[UIScrollView alloc] init];
        //scrollView.backgroundColor = [UIColor grayColor ];
        scrollViewForState.frame = CGRectMake(0, 270, screenWidth , screenHeight-270);
        scrollViewForState.backgroundColor = [UIColor blackColor];
        CGFloat finalheight = 0;
        
    for (int i = 0; i < dataArray.count; i++) {
            
            CGFloat heightOfText = [self getTextHeightByWidth:[[dataArray objectAtIndex:i]valueForKey:@"LinkName"] textFont:[UIFont fontWithName:@"Lato-Regular" size:17.0f] textWidth:screenWidth-30];
        
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.tag = i;
            [button addTarget:self
                       action:@selector(segBtnTapped:)
             forControlEvents:UIControlEventTouchUpInside];
            button.layer.cornerRadius = 4;
        
           if (heightOfText >21) {
           [button setFrame:CGRectMake(15,finalheight, screenWidth-30 , 60)];
           }else {
           [button setFrame:CGRectMake(15,finalheight, screenWidth-30 , 40)];
           }
           [button setTitle:[[dataArray objectAtIndex:i]valueForKey:@"LinkName"] forState:UIControlStateNormal];
            [button.titleLabel setTextAlignment:NSTextAlignmentCenter];
                   button.titleLabel.numberOfLines = 2;
            button.titleLabel.adjustsFontSizeToFitWidth = YES;
            button.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:17.0f];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor orangeColor]];

            
            
           
            [scrollViewForState addSubview:button];
            finalheight= finalheight + button.frame.size.height+15;
        }
        
        
       scrollViewForState.showsVerticalScrollIndicator = NO;
       scrollViewForState.backgroundColor = [UIColor whiteColor];
        
        scrollViewForState.contentSize = CGSizeMake(screenWidth, finalheight+100);
        [self.view addSubview:scrollViewForState];
        

    
    
    
}

- (IBAction)segBtnTapped:(id)sender {
  int index = [sender tag] ;
    
    WebCheckInVC *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"WebCheckInVC"];
    myView.webLink = [[statedataArray objectAtIndex:index] valueForKey:@"Link"];
    myView.headerStr = [[statedataArray objectAtIndex:index] valueForKey:@"LinkName"];


    [self.navigationController pushViewController:myView animated:true];
}

- (CGFloat)getTextHeightByWidth:(NSString*)text textFont:(UIFont*)textFont textWidth:(float)textWidth {
    
    if (!text) {
        return 0;
    }
    CGSize boundingSize = CGSizeMake(textWidth, CGFLOAT_MAX);
    NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:text attributes:@{ NSFontAttributeName: textFont }];
    
    CGRect rect = [attributedText boundingRectWithSize:boundingSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    CGSize requiredSize = rect.size;
    return requiredSize.height;
}



-(void)apiCallForUSA {
     
    [SVProgressHUD showWithStatus:@"Loading.." maskType:SVProgressHUDMaskTypeBlack];
    
    
    NSDictionary *parameters = @{
          @"CountryName": @"United States",
          @"CountryCode": @"US",
          @"StateCode":@""
          
      };

      NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];

      NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://testtravelapi.azurewebsites.net/v1/HealthAdvisory/GetPublicAdvisory"]];
      [request setHTTPMethod:@"POST"];
      [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"content-type"];

      NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
      NSURLSessionUploadTask *dataTask = [session uploadTaskWithRequest: request
              fromData:data completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
          NSLog(@"%@", json);
         [SVProgressHUD dismiss];
          
          if (json) {
          
                        statedataArray = [json valueForKey:@"ListOfAdvisories"];
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self drawstateOfUSA:statedataArray];
              });
         
                        }
          }];

      [dataTask resume];
}
                                          
    

@end
