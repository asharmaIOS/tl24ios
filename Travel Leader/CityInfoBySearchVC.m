//
//  CityInfoBySearchVC.m
//  Travel Leader
//
//  Created by Abhishek Sharma on 1/2/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "CityInfoBySearchVC.h"
#import "MapVC.h"
#import "CityDetailCell.h"
#import "SideSeenImageCell.h"
#import "SearchDestinationVC.h"
#import "TravelConstantClass.h"
#import "SideSeenCell.h"
#import "HorizantalCell.h"
#import "XMLReader.h"
#import "SVProgressHUD.h"
#import "UIFont+Extras.h"
#import "DemoMessagesViewController.h"
#import "ValidationAndJsonVC.h"



#import "ReadMoreDescriptionOfdestinationViewController.h"

@interface CityInfoBySearchVC ()<UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    UIScrollView *scrollView;
    NSMutableArray *segmentArrayOfButtons;
    NSArray *completeDataArray;
    NSMutableArray *dataForReadmore;
    UIScrollView *scrollViewForSepcificData;
    NSString *strCityNameWithState;
    NSString *strCityId;
    UIButton *mapButton;
    int segmentTag;
    
    
    NSDictionary *topPlaceEatDic;
    NSDictionary *hiddenGemDic;
}

@property (weak, nonatomic) IBOutlet UILabel *citynamelbl;

@end

@implementation CityInfoBySearchVC

- (void)viewDidLoad {
    
    [self.navigationController.navigationBar setHidden: NO];
    self.navigationController.navigationBar.barTintColor=[UIColor orangeColor];
      
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
//           mapButton  = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth - 90,screenHeight -90, 67, 67)];
//           [mapButton addTarget:self action:@selector(mapButtonTap) forControlEvents:UIControlEventTouchUpInside];
//           [mapButton setBackgroundImage:[UIImage imageNamed:@"location.png"] forState:UIControlStateNormal];
//           [[[[UIApplication sharedApplication] delegate]window] addSubview:mapButton];
//
    segmentArrayOfButtons = [[NSMutableArray alloc] init];
    dataForReadmore = [[NSMutableArray alloc] init];
    
    
    if (_cityDic.allKeys!=NULL) {
        
        NSString *imageUrl = [[[_cityDic valueForKey:@"images"] firstObject]valueForKey:@"source_url"];
        
        strCityNameWithState = [[_cityDic valueForKey:@"names"] firstObject];
        strCityId = [_cityDic valueForKey:@"id"];
        [self loadMainImage:imageUrl];
        NSArray *segmentArray = [NSArray arrayWithObjects:strCityNameWithState,@"Find Hidden Gems",@"Top Places To Eat", nil];
        [self drawSegmentController:segmentArray];
        [self loadCityInfo:_cityDic];
        
    }else {
        
        [ValidationAndJsonVC displayAlert:@"Data not Found!" HeaderMsg:@"OOPS"];
        
    }
    
    
     [self setupLeftMenuButton];
    
    
    
    [super viewDidLoad];
}

-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 22, 16)];
    [backButton addTarget:self action:@selector(backButtonTapp:) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
  //  self.navigationItem.leftBarButtonItems = @[searchItem];
    
    
    
    
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc] init];
    textButton1.title = @"";
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}

-(void)flipVie
{
    DemoMessagesViewController *myView = [self.storyboard instantiateViewControllerWithIdentifier:@"chatvc"];
    
    [self.navigationController pushViewController:myView animated:true];
}

    


- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController.navigationBar setHidden: NO];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    mapButton  = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth - 90,screenHeight -90, 67, 67)];
    [mapButton addTarget:self action:@selector(mapButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [mapButton setBackgroundImage:[UIImage imageNamed:@"location.png"] forState:UIControlStateNormal];
    [[[[UIApplication sharedApplication] delegate]window] addSubview:mapButton];

   // [self loadDataAccordingCity];
}

-(void)mapButtonTap {
    [mapButton removeFromSuperview];

    
  
    MapVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
    vc.strForCity = strCityId;
    vc.cityDic =_cityDic;
    
    
    [self.navigationController pushViewController:vc animated:YES];
    


}

-(void)backButtonTapp:(id)sender {
    
    [mapButton removeFromSuperview];

    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)drawForPlacesToEat:(NSDictionary *)dataDic {
   
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat finalHeight = 0;
    
    //[dataForReadmore removeAllObjects];
    
    if (!scrollViewForSepcificData) {
        scrollViewForSepcificData = [[UIScrollView alloc] init];
        scrollViewForSepcificData.frame = CGRectMake(0, 345, screenWidth , 650);
        [self.view  addSubview: scrollViewForSepcificData];
    }
    

    for(UIView *subview in [scrollViewForSepcificData subviews]) {
        [subview removeFromSuperview];
    }
    
    scrollViewForSepcificData.contentOffset = CGPointMake(0,0);
    
    NSArray *arrayOfRestaurents = [dataDic valueForKey:@"results"];
    
    
    for (int i =0 ; i<arrayOfRestaurents.count;i++) {
    
        CGFloat viewHeight = 0;
    NSDictionary *dataDic = [arrayOfRestaurents objectAtIndex:i];
    
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.frame = CGRectMake(0, finalHeight+10, screenWidth , 150);
    backgroundView.backgroundColor = [UIColor whiteColor];
//    backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    backgroundView.layer.borderWidth = 1.0f;
    [scrollViewForSepcificData addSubview: backgroundView];
    
    UILabel *restaurentnamelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 8, 280, 20)];
    restaurentnamelabel.font =[UIFont fontWithName:@"Lato-Bold" size:18.0f];
    restaurentnamelabel.textAlignment = NSTextAlignmentLeft;
    restaurentnamelabel.text = [dataDic valueForKey:@"name"];
    [backgroundView addSubview:restaurentnamelabel];
       
        UILabel *ratinglabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 33, 280, 20)];
        ratinglabel.font = [UIFont fontWithName:@"Lato" size:14.0f];
        ratinglabel.textColor = [UIColor orangeColor];
        ratinglabel.textAlignment = NSTextAlignmentLeft;
        
        NSString *stringRating = [NSString stringWithFormat:@"Rating: %.1f out of 10",[[dataDic valueForKey:@"score"]floatValue]];
        ratinglabel.text = stringRating;
        [backgroundView addSubview:ratinglabel];
        
     
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        
        finalHeight = finalHeight +55;
        viewHeight = 55;
        NSArray *imageArray = [dataDic valueForKey:@"images"];
        if ([imageArray count] >0) {
          
        
       // NSString *imageStr = [[[[[dataDic valueForKey:@"images"] lastObject]valueForKey:@"sizes" ] valueForKey:@"medium"] valueForKey:@"url"];
        
        UIScrollView *horizontalScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 60, screenWidth, 170)];
            [backgroundView addSubview:horizontalScroll];
        
        for (int j=0; j<imageArray.count; j++) {
        NSString *imageStr = [[[[[dataDic valueForKey:@"images"] objectAtIndex:j]valueForKey:@"sizes" ] valueForKey:@"medium"] valueForKey:@"url"];
        UIImageView *imageview = [[UIImageView alloc]
                                  initWithFrame:CGRectMake(240*j+10, 0, 230, 170)];
            
            imageview.layer.borderColor = [UIColor lightGrayColor].CGColor;
            imageview.layer.borderWidth = 1.0f;
        
            dispatch_async(dispatch_get_main_queue(), ^{
                  
        [self downloadImageWithURL:[NSURL URLWithString: imageStr] completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                
                imageview.image = image;
            }
              }];
            
            });
            
        [horizontalScroll addSubview:imageview];
        horizontalScroll.showsHorizontalScrollIndicator = NO;
            
        
        
        }
            horizontalScroll.contentSize = CGSizeMake(250*imageArray.count, 170);
            
            viewHeight = 245;
            finalHeight = finalHeight+190;
        }
     
        NSArray *proprtyArray = [dataDic valueForKey:@"properties"];
        if (proprtyArray.count >0) {

            for (int k=0; k<proprtyArray.count; k++) {

            NSDictionary *proprtydic = [proprtyArray objectAtIndex:k];
           if ([[proprtydic valueForKey:@"name"] isEqualToString:@"Address"]) {

               UILabel *addresslabel = [[UILabel alloc]initWithFrame:CGRectMake(10, viewHeight, screenWidth, 20)];
               addresslabel.font = [UIFont fontWithName:@"Lato" size:14.0f];
               addresslabel.adjustsFontSizeToFitWidth =YES;

               addresslabel.textAlignment = NSTextAlignmentLeft;
               NSString *addressstr = [NSString stringWithFormat:@"Address: %@",[proprtydic valueForKey:@"value"]];
               addresslabel.text = addressstr;
               [backgroundView addSubview:addresslabel];
               
               UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openAddress:)];
               lblAction.delegate = self;
               lblAction.numberOfTapsRequired = 1;
               addresslabel.userInteractionEnabled = YES;
               addresslabel.tag = i ;
               [addresslabel addGestureRecognizer:lblAction];
               
               
               
               viewHeight = viewHeight + 30;
               finalHeight = finalHeight +30;

           }

                
                if ([[proprtydic valueForKey:@"name"] isEqualToString:@"Phone"]) {

                    UILabel *addresslabel = [[UILabel alloc]initWithFrame:CGRectMake(10, viewHeight, screenWidth, 20)];
                    addresslabel.font = [UIFont fontWithName:@"Lato" size:14.0f];
                    addresslabel.adjustsFontSizeToFitWidth =YES;

                    addresslabel.textAlignment = NSTextAlignmentLeft;
                    NSString *addressstr = [NSString stringWithFormat:@"Phone: %@",[proprtydic valueForKey:@"value"]];
                    addresslabel.text = addressstr;
                    [backgroundView addSubview:addresslabel];
                    
                    UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(phoneCall:)];
                    lblAction.delegate = self;
                    lblAction.numberOfTapsRequired = 1;
                    addresslabel.userInteractionEnabled = YES;
                    addresslabel.tag = i ;
                    [addresslabel addGestureRecognizer:lblAction];
                    
                    
                    viewHeight = viewHeight+30;
                    finalHeight = finalHeight +30;

                }
                
                
                
                if ([[proprtydic valueForKey:@"name"] isEqualToString:@"Website"]) {
                    
                    UITextView *addresstext = [[UITextView alloc]initWithFrame:CGRectMake(5, viewHeight-5, screenWidth-10, 20)];
                    addresstext.font = [UIFont fontWithName:@"Lato" size:14.0f];
                    //addresslabel.adjustsFontSizeToFitWidth =YES;
                    addresstext.textAlignment = NSTextAlignmentLeft;
                    addresstext.userInteractionEnabled = NO;
                    NSString *addressstr = [NSString stringWithFormat:@"Website: %@",[proprtydic valueForKey:@"value"]];
                    addresstext.text = addressstr;
                    [backgroundView addSubview:addresstext];
                    
                    NSString *textViewText = addresstext.text;
                    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
                    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                    paragraphStyle.lineSpacing = 7;
                    NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont fontWithName:@"Lato" size:14.0f] };
                    [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
                    
                    addresstext.attributedText = attributedString;
                    CGRect frame = addresstext.frame;
                    frame.size.height = addresstext.contentSize.height+10;
                    addresstext.frame = frame;
                    
                    
                    viewHeight = viewHeight+frame.size.height;
                    finalHeight = finalHeight +frame.size.height;
                
                    
                    UITapGestureRecognizer *lblAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(websiteOpen:)];
                    lblAction.delegate = self;
                    lblAction.numberOfTapsRequired = 1;
                    addresstext.userInteractionEnabled = YES;
                    addresstext.tag = i ;
                    [addresstext addGestureRecognizer:lblAction];
                    
                
                }
                
                if ([[proprtydic valueForKey:@"name"] isEqualToString:@"Hours"]){
                    
                    
                    UITextView *addresstext = [[UITextView alloc]initWithFrame:CGRectMake(5, viewHeight-5, screenWidth-10, 20)];
                    addresstext.font = [UIFont systemFontOfSize:14];
                    //addresslabel.adjustsFontSizeToFitWidth =YES;
                    addresstext.textAlignment = NSTextAlignmentLeft;
                    addresstext.userInteractionEnabled = NO;
                    NSString *addressstr = [NSString stringWithFormat:@"Hours: %@",[proprtydic valueForKey:@"value"]];
                    addresstext.text = addressstr;
                    [backgroundView addSubview:addresstext];
                    
                    
                    NSString *textViewText = addresstext.text;
                    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
                    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                    paragraphStyle.lineSpacing = 7;
                    NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont fontWithName:@"Lato" size:14.0f] };
                    [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
                    
                    addresstext.attributedText = attributedString;
                    CGRect frame = addresstext.frame;
                    frame.size.height = addresstext.contentSize.height+5;
                    addresstext.frame = frame;
                
                    
                    
                    viewHeight = viewHeight+frame.size.height;
                    finalHeight = finalHeight +frame.size.height;
                    

                }


            }

        }
        
        NSString *snniptStr = [dataDic valueForKey:@"snippet"];
        
        if (snniptStr.length>1) {
            
            UITextView *addresstext = [[UITextView alloc]initWithFrame:CGRectMake(5, viewHeight-5, screenWidth-10, 20)];
            addresstext.font = [UIFont fontWithName:@"Lato" size:14.0f];
            //addresslabel.adjustsFontSizeToFitWidth =YES;
            addresstext.textAlignment = NSTextAlignmentLeft;
            addresstext.userInteractionEnabled = NO;
            NSString *addressstr = [NSString stringWithFormat:@"Snippet: %@",snniptStr];
            addresstext.text = addressstr;
            [backgroundView addSubview:addresstext];
            
            
            NSString *textViewText = addresstext.text;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineSpacing = 7;
            NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont fontWithName:@"Lato" size:14.0f] };
            [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
            
            addresstext.attributedText = attributedString;
            CGRect frame = addresstext.frame;
            frame.size.height = addresstext.contentSize.height+10;
            addresstext.frame = frame;
            
            
            
            viewHeight = viewHeight+frame.size.height;
            finalHeight = finalHeight +frame.size.height;
            
        }
        
        
        
        UIButton *fbbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [fbbutton addTarget:self
                   action:@selector(fbMethod:)
         forControlEvents:UIControlEventTouchUpInside];
        fbbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

        fbbutton.tag = i;
       // fbbutton.titleLabel.textColor = [UIColor blueColor];
        [fbbutton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        fbbutton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];

        [fbbutton setTitle:@"View on facebook" forState:UIControlStateNormal];
        fbbutton.frame = CGRectMake(10, viewHeight-10, 160.0, 20);
        [backgroundView addSubview:fbbutton];
        
        UIButton *mapbutton = [UIButton buttonWithType:UIButtonTypeCustom];
        [mapbutton addTarget:self
                   action:@selector(mapMethod:)
         forControlEvents:UIControlEventTouchUpInside];
        mapbutton.tag = i;
        mapbutton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        mapbutton.backgroundColor = [UIColor clearColor];
        [mapbutton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        mapbutton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:16.0f];
       // mapbutton.titleLabel.textColor = [UIColor redColor];
        [mapbutton setTitle:@"View on map" forState:UIControlStateNormal];
        mapbutton.frame = CGRectMake(screenWidth-170, viewHeight-10, 160.0, 20);
        [backgroundView addSubview:mapbutton];
        
        viewHeight = viewHeight+35;
        finalHeight = finalHeight +35;
            

        UILabel *graylabel = [[UILabel alloc]initWithFrame:CGRectMake(0, viewHeight-3, screenWidth, 1)];
        graylabel.backgroundColor = [UIColor grayColor];
        [backgroundView addSubview:graylabel];
        
        CGRect newFrame = backgroundView.frame;
        newFrame.size.height = viewHeight;
        [backgroundView setFrame:newFrame];
        
       
       
//        backgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        backgroundView.layer.borderWidth = 1.0f;
    }
    scrollViewForSepcificData.contentSize = CGSizeMake(screenWidth,  finalHeight+150);
    
    
}


-(void)fbMethod :(UIButton*)sender {
    int index= sender.tag;
    NSArray *arrayOfRestaurents;
    
    if (segmentTag ==1) {
        
        arrayOfRestaurents = [hiddenGemDic valueForKey:@"results"];
    }
    
    else {
        arrayOfRestaurents = [topPlaceEatDic valueForKey:@"results"];
        
    }
   
    NSString *fbStr = [[arrayOfRestaurents objectAtIndex:index] valueForKey:@"facebook_id"];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]]) {
       
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",fbStr]] options:@{} completionHandler:nil];
       
    }
    else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.facebook.com/%@",fbStr]] options:@{} completionHandler:nil];
      
    }
    
}


-(void)mapMethod :(UIButton*)sender {
    
    int index= sender.tag;
  
    NSArray *arrayOfRestaurents;
    
    if (segmentTag ==1) {
        
        arrayOfRestaurents = [hiddenGemDic valueForKey:@"results"];
    }
    
    else {
        arrayOfRestaurents = [topPlaceEatDic valueForKey:@"results"];
        
    }
   
    NSString *letStr = [[[arrayOfRestaurents objectAtIndex:index] valueForKey:@"coordinates"] valueForKey:@"latitude"];
    NSString *longStr = [[[arrayOfRestaurents objectAtIndex:index] valueForKey:@"coordinates"]valueForKey:@"longitude" ];

//    NSString *mapStr = [NSString stringWithFormat:@"https://www.google.co.in/maps/dir/?saddr=&daddr=\%.3f,\%.3f&directionsmode=driving",[letStr doubleValue],[longStr doubleValue]];
    
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        
     
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=&daddr=%.3f,%.3f&directionsmode=driving",[letStr doubleValue],[longStr doubleValue]]] options:@{} completionHandler:nil];
     // [[UIApplication sharedApplication] openURL:
     //  [NSURL URLWithString:@"comgooglemaps://?center=40.765819,-73.975866&zoom=14&views=traffic"]];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.google.co.in/maps/dir/?saddr=&daddr=\%.3f,\%.3f&directionsmode=driving",[letStr doubleValue],[longStr doubleValue]]] options:@{} completionHandler:nil];
        
   
    }
    
    

    }
   
-(void)apiCalltopPlacesEat:(NSString *)citystr{

    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeBlack];

__block NSMutableDictionary *resultsDictionary;

NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"", @"", nil];//if your json structure is something like {"title":"first title","blog_id":"1"}
//if ([NSJSONSerialization isValidJSONObject:userDictionary]) {//validate it
NSError* error;
NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userDictionary options:NSJSONWritingPrettyPrinted error: &error];
NSString *urlstring = [NSString stringWithFormat:@"https://testtravelapi.azurewebsites.net/api/bifrost/POItoEats?cityName=%@",citystr];
    
NSURL* url = [NSURL URLWithString:urlstring];
NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
[request setHTTPMethod:@"POST"];//use POST
[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
[request setValue:[NSString stringWithFormat:@"%d",[jsonData length]] forHTTPHeaderField:@"Content-length"];
[request setHTTPBody:jsonData];//set data
 __block NSError *error1 = [[NSError alloc] init];

 //use async way to connect network
[NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
{
    if ([data length]>0 && error == nil) {
        resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
        NSLog(@"resultsDictionary is %@",resultsDictionary);
       // cityarr = [resultsDictionary valueForKey:@"results"];
        topPlaceEatDic = resultsDictionary;
       
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self drawForPlacesToEat:resultsDictionary];
       
        });
        
      
        
      
       
      
                
        

    } else if ([data length]==0 && error ==nil) {
        NSLog(@" download data is null");
        [SVProgressHUD dismiss];
    } else if( error!=nil) {
        [SVProgressHUD dismiss];
        NSLog(@" error is %@",error);
    }
}];
    
}


-(void)apiCallHiddenGems: (NSString *)citystr{

    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeBlack];

__block NSMutableDictionary *resultsDictionary;

NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"", @"", nil];//if your json structure is something like {"title":"first title","blog_id":"1"}
//if ([NSJSONSerialization isValidJSONObject:userDictionary]) {//validate it
NSError* error;
NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userDictionary options:NSJSONWritingPrettyPrinted error: &error];
NSString *urlstring = [NSString stringWithFormat:@"https://testtravelapi.azurewebsites.net/api/bifrost/POIhiddenGems?cityName=%@",citystr];
    
NSURL* url = [NSURL URLWithString:urlstring];
NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
[request setHTTPMethod:@"POST"];//use POST
[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
[request setValue:[NSString stringWithFormat:@"%d",[jsonData length]] forHTTPHeaderField:@"Content-length"];
[request setHTTPBody:jsonData];//set data
 __block NSError *error1 = [[NSError alloc] init];

 //use async way to connect network
[NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
{
    if ([data length]>0 && error == nil) {
        resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
        NSLog(@"resultsDictionary is %@",resultsDictionary);
       // cityarr = [resultsDictionary valueForKey:@"results"];
        hiddenGemDic = resultsDictionary;
        
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self drawForPlacesToEat:resultsDictionary];
       
        });
       
       
        
       
        
      
       
      
                
        

    } else if ([data length]==0 && error ==nil) {
        NSLog(@" download data is null");
        [SVProgressHUD dismiss];
    } else if( error!=nil) {
        [SVProgressHUD dismiss];
        NSLog(@" error is %@",error);
    }
}];
    
}


- (void)loadMainImage:(NSString *)mainImageStr {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
//CGFloat screenHeight = screenRect.size.height;
    
    UIImageView *imageview = [[UIImageView alloc]
                              initWithFrame:CGRectMake(0, 44, screenWidth, 250)];
   // UIImage *imageForMain = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator startAnimating];
    [indicator setCenter:imageview.center];
    [imageview addSubview:indicator];
    
    
    [self downloadImageWithURL:[NSURL URLWithString: mainImageStr] completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
          
            imageview.image = image;
            [indicator removeFromSuperview];
            [self loadTextOverMainImageView:imageview];

        }
    }];
    
    
   // imageview.image = mainImage;
    [imageview setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:imageview];
    
}

-(void)loadTextOverMainImageView:(UIImageView *)imageView{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    UILabel *namelabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 125, screenWidth-40, 22)];
    namelabel.textColor = [UIColor whiteColor];
    namelabel.font = [UIFont systemFontOfSize:18];
    namelabel.textAlignment = NSTextAlignmentCenter;
    namelabel.text = @"WELCOME TO";
    [imageView addSubview:namelabel];
    
    
    UILabel *cityNameLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 145, screenWidth-40, 30)];
    cityNameLbl.textColor = [UIColor whiteColor];
    cityNameLbl.font = [UIFont boldSystemFontOfSize:26];
    cityNameLbl.textAlignment = NSTextAlignmentCenter;
    cityNameLbl.text = strCityNameWithState;
    [imageView addSubview:cityNameLbl];
    
    
}





-(NSMutableArray *)arrayOfSegment:(NSArray *)dataArray {
    
    NSMutableArray *segmentArray = [[NSMutableArray alloc]init ];
    
    for (int i= 0; i<dataArray.count; i++) {
        [segmentArray addObject:[[dataArray objectAtIndex:i ]valueForKey:@"name"]];
    }
    
    return segmentArray;
}

-(void)drawSegmentController:(NSArray *)dataArray {

    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    for(UIView *subview in [scrollView subviews]) {
        [subview removeFromSuperview];
    }
    [segmentArrayOfButtons removeAllObjects];
    
    scrollView = [[UIScrollView alloc] init];
    //scrollView.backgroundColor = [UIColor grayColor ];
    scrollView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:0.7];
    scrollView.frame = CGRectMake(0, 295, screenWidth , 55);
    
    CGFloat finalWidth = 10;
    
    for (int i = 0; i <dataArray.count ; i++) {
        
        CGSize stringsize = [ [dataArray objectAtIndex:i] sizeWithFont:[UIFont systemFontOfSize:17]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.tag = i;
        button.titleLabel.font = [UIFont systemFontOfSize:17];
        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        if (i==0) {
            
            [button setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
             
        }

        [button addTarget:self
                   action:@selector(buttonClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[dataArray objectAtIndex:i] forState:UIControlStateNormal];
       
        [button setFrame:CGRectMake(finalWidth,05,stringsize.width, 45)];
         [scrollView addSubview:button];
        [segmentArrayOfButtons addObject:button ];
        finalWidth = finalWidth + button.frame.size.width+ 35;
    }
    

    scrollView.showsHorizontalScrollIndicator = NO;

    scrollView.contentSize = CGSizeMake(finalWidth, 40);
    [self.view addSubview:scrollView];
    
}

-(void) readMoreBtnClick:(UIButton*)sender {
    //dataForReadmore
    if (dataForReadmore.count!=0) {
        NSDictionary *dic = [dataForReadmore objectAtIndex:sender.tag];
        
       // ReadMoreDescriptionOfdestinationViewController *vc  = [[ReadMoreDescriptionOfdestinationViewController alloc] initWithNibName:@"ReadMoreDescriptionOfdestinationViewController" bundle:nil];
        
        ReadMoreDescriptionOfdestinationViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadMoreDescriptionOfdestinationViewController"];

        vc.dataDic =dic;
        [self.navigationController pushViewController:vc animated:YES];
    }
   
}


-(void) buttonClicked:(UIButton*)sender
{
    NSLog(@"you clicked on button %d", sender.tag);
    
    
    for (int i = 0; i<segmentArrayOfButtons.count; i++) {
        
        if (i == sender.tag) {
             [[segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        }
        else {
           [ [segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        

    }
    
    
    if (sender.tag==0) {
        segmentTag =0;
      
        [self loadCityInfo:_cityDic];
    }
    
    else if (sender.tag==1) {
        
        segmentTag =1;

        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self apiCallHiddenGems:strCityId];
       
        });
       
       
    }else if (sender.tag==2) {
        segmentTag =2;

        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self apiCalltopPlacesEat:strCityId];
       
        });
       
    }
    
  //  UIView *view; //View to center
   // UIScrollView *scrollView; //scroll view
    
    CGPoint point = CGPointMake(sender.frame.origin.x + sender.frame.size.width / 2,
                                sender.frame.origin.y + sender.frame.size.height / 2);
    
    CGRect rectToZoom = CGRectMake(point.x * scrollView.zoomScale - roundf(scrollView.frame.size.width /2.),
                                   point.y * scrollView.zoomScale - roundf(scrollView.frame.size.height /2.),
                                   scrollView.frame.size.width,
                                   scrollView.frame.size.height);
    
    [scrollView scrollRectToVisible:rectToZoom animated:YES];
    
 
     // [self loadDataAccordingButtonSelect:completeDataArray indexOfbuttonSelect:sender.tag];
}

-(NSString *) stringByStrippingHTML :(NSString *)str {
  NSRange r;
 // NSString *s = [self copy] ;
    NSString *s = str ;
  while ((r = [s rangeOfString:@"<[^>]*>" options:NSRegularExpressionSearch]).location != NSNotFound)
    s = [s stringByReplacingCharactersInRange:r withString:@""];
  return s;
}

-(void)loadCityInfo :(NSDictionary *)dataDic {
    
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat finalHeight = 0;
    
    [dataForReadmore removeAllObjects];
    
    if (!scrollViewForSepcificData) {
        scrollViewForSepcificData = [[UIScrollView alloc] init];
        scrollViewForSepcificData.frame = CGRectMake(0, 345, screenWidth , 650);
        [self.view  addSubview: scrollViewForSepcificData];
    }
    

    for(UIView *subview in [scrollViewForSepcificData subviews]) {
        [subview removeFromSuperview];
    }
    
    scrollViewForSepcificData.contentOffset = CGPointMake(0,0);
    
    
    UITextView *textView = [[UITextView alloc]init ];

    textView.frame = CGRectMake(7, 7, screenWidth-14, 300);
    
    
    NSDictionary *dic = [[[dataDic valueForKey:@"structured_content"] valueForKey:@"sections"]firstObject];
       // textView.text = [[dataArray objectAtIndex:index] valueForKey:@"description"];
    NSString *completeIntroStr = [NSString stringWithFormat:@"%@ %@", [dic valueForKey:@"body"],[dic valueForKey:@"summary"] ];
    NSString *stripeStr = [self stringByStrippingHTML:completeIntroStr];
   // NSString *removeHtml = [completeIntroStr stripe ]
   
 NSString  *finalStr = [stripeStr stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    textView.text = finalStr;

//    CGRect frame = textView.frame;
//    frame.size.height = textView.contentSize.height;
//    textView.frame = frame;
    textView.editable = false;
    textView.showsVerticalScrollIndicator = false;

//    textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    textView.layer.borderWidth = 1.0f;
    
    NSString *textViewText = textView.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:textViewText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 7;
    NSDictionary *dict = @{NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : [UIFont fontWithName:@"Lato-Regular" size:17.0f] };
    [attributedString addAttributes:dict range:NSMakeRange(0, [textViewText length])];
    
    textView.attributedText = attributedString;
    CGRect frame = textView.frame;
    frame.size.height = textView.contentSize.height+20;
    textView.frame = frame;
   // textView.frame = CGRectMake(07, 05, screenWidth-14, textView.contentSize.height);
    [scrollViewForSepcificData addSubview:textView];
    
    //scrollViewForSepcificData.contentSize =
  
    scrollViewForSepcificData.contentSize = CGSizeMake(screenWidth, textView.contentSize.height+20);


  
    
      //  finalHeight = textView.contentSize.height;
        
   
    
}



- (void)openAddress:(UITapGestureRecognizer *)tapGesture {
    
    UILabel *label = (UILabel *)tapGesture.view;
    // NSLog(@"Lable tag is ::%ld",(long)label.tag);
    
    NSArray *arrayOfRestaurents;
    
    if (segmentTag ==1) {
        
        arrayOfRestaurents = [hiddenGemDic valueForKey:@"results"];
    }
    
    else {
        arrayOfRestaurents = [topPlaceEatDic valueForKey:@"results"];
        
    }
   
    
    
     NSString *letStr = [[[arrayOfRestaurents objectAtIndex:label.tag] valueForKey:@"coordinates"] valueForKey:@"latitude"];
     NSString *longStr = [[[arrayOfRestaurents objectAtIndex:label.tag] valueForKey:@"coordinates"]valueForKey:@"longitude" ];

     
     if ([[UIApplication sharedApplication] canOpenURL:
          [NSURL URLWithString:@"comgooglemaps://"]]) {
         
      
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=&daddr=%.3f,%.3f&directionsmode=driving",[letStr doubleValue],[longStr doubleValue]]] options:@{} completionHandler:nil];
      // [[UIApplication sharedApplication] openURL:
      //  [NSURL URLWithString:@"comgooglemaps://?center=40.765819,-73.975866&zoom=14&views=traffic"]];
     } else {
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.google.co.in/maps/dir/?saddr=&daddr=\%.3f,\%.3f&directionsmode=driving",[letStr doubleValue],[longStr doubleValue]]] options:@{} completionHandler:nil];
         
    
     }
     
     
    
    
    
    
//    NSString *addressStr ;
//    NSArray *propertyArr = [[arrayOfRestaurents objectAtIndex:label.tag] valueForKey:@"properties"];
//    if (propertyArr.count >0) {
//
//                for (int k=0; k<propertyArr.count; k++) {
//
//                NSDictionary *proprtydic = [propertyArr objectAtIndex:k];
//               if ([[proprtydic valueForKey:@"name"] isEqualToString:@"Address"]) {
//
//                   addressStr = [proprtydic valueForKey:@"value"];
//               }
//
//            }
//
//    }
//
//    if (addressStr) {
//    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"comgooglemaps://"]]) {
//        NSString *urlString = [NSString stringWithFormat:@"comgooglemaps://?q=%@",addressStr];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%@",addressStr]] options:@{} completionHandler:nil];
//    } else {
//        NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@",addressStr];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@",addressStr]] options:@{} completionHandler:nil];
//
//    }
//    }
}

- (void)phoneCall:(UITapGestureRecognizer *)tapGesture {
    UILabel *label = (UILabel *)tapGesture.view;
   // NSLog(@"Lable tag is ::%ld",(long)label.tag);
    
    NSArray *arrayOfRestaurents;
    
    if (segmentTag ==1) {
        
        arrayOfRestaurents = [hiddenGemDic valueForKey:@"results"];
    }
    
    else {
        arrayOfRestaurents = [topPlaceEatDic valueForKey:@"results"];
        
    }
   
    NSString *phoneStr ;
    NSArray *propertyArr = [[arrayOfRestaurents objectAtIndex:label.tag] valueForKey:@"properties"];
    if (propertyArr.count >0) {

                for (int k=0; k<propertyArr.count; k++) {

                NSDictionary *proprtydic = [propertyArr objectAtIndex:k];
               if ([[proprtydic valueForKey:@"name"] isEqualToString:@"Phone"]) {
                 
                   phoneStr = [proprtydic valueForKey:@"value"];
               }

            }
        
    }
    NSString *phoneNumberStr = [phoneStr stringByReplacingOccurrencesOfString:@" " withString:@""];

     //NSString *phoneNumberStr = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt://%@",phoneNumberStr]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
       UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
    
}


- (void)websiteOpen:(UITapGestureRecognizer *)tapGesture {
    UITextView *label = (UITextView*)tapGesture.view;
    // NSLog(@"Lable tag is ::%ld",(long)label.tag);
    
    NSArray *arrayOfRestaurents;
    
    if (segmentTag ==1) {
        
        arrayOfRestaurents = [hiddenGemDic valueForKey:@"results"];
    }
    
    else {
        arrayOfRestaurents = [topPlaceEatDic valueForKey:@"results"];
        
    }
   
    NSString *webStr ;
    NSArray *propertyArr = [[arrayOfRestaurents objectAtIndex:label.tag] valueForKey:@"properties"];
    if (propertyArr.count >0) {

                for (int k=0; k<propertyArr.count; k++) {

                NSDictionary *proprtydic = [propertyArr objectAtIndex:k];
               if ([[proprtydic valueForKey:@"name"] isEqualToString:@"Website"]) {
                 
                   webStr = [proprtydic valueForKey:@"value"];
               }

            }
        
    }
    
    
    NSString *websiteStr = [webStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    
    [self openScheme:websiteStr];
   
}

- (void)openScheme:(NSString *)scheme {
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:scheme];
    
    if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
        [application openURL:URL options:@{}
           completionHandler:^(BOOL success) {
               NSLog(@"Open %@: %d",scheme,success);
           }];
    } else {
        BOOL success = [application openURL:URL];
        NSLog(@"Open %@: %d",scheme,success);
    }
}


- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    
    if (url!= nil ){
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
  }
}
    
  

@end
