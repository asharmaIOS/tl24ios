//
//  RestrictionsandStatsVC.h
//  Travel Leader
//
//  Created by Abhishek Sharma on 28/08/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RestrictionsandStatsVC : UIViewController

@property (strong, atomic) NSDictionary *dataDic;

@end

NS_ASSUME_NONNULL_END
