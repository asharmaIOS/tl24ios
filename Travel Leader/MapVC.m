//
//  MapVC.m
//  Travel Leader
//
//  Created by wFares Dev on 3/4/20.
//  Copyright © 2020 Gurpreet Singh. All rights reserved.
//

#import "MapVC.h"
#import "SVProgressHUD.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ValidationAndJsonVC.h"

@interface MapVC () <GMSMapViewDelegate,CLLocationManagerDelegate>

{
    CGFloat currentZoom;

    UIScrollView *scrollView;
    GMSCameraPosition *cameraPosition;
    CLLocationManager *locationManager;
    NSString *lattitude;
    NSString *longitude;

     
    int segmentTag;
    GMSMapView *mapView;
    int indexOfDay;
}
@property (strong, nonatomic) GMSMapView *mapView;

@property (strong, nonatomic)  NSMutableArray *legsDataArray;

@property (strong, nonatomic)  NSMutableArray *completedaysArray;
@property (strong, nonatomic)  NSMutableArray *segmentArrayOfButtons;

@property (strong, nonatomic)  NSMutableArray *completeDataArray;
@property (strong, nonatomic)  NSMutableArray *letArray;
@property (strong, nonatomic)  NSMutableArray *longArray;
@end

@implementation MapVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    _mapView.delegate = self;
    _segmentArrayOfButtons = [[NSMutableArray alloc] init];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = 200;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    [locationManager requestAlwaysAuthorization];
    [locationManager requestWhenInUseAuthorization];
    
    
    if([CLLocationManager authorizationStatus]) {
        
        [locationManager startUpdatingLocation];
        
    }
    
//    [_segmentArrayOfButtons addObject:@"Local Highlights"];
//    [_segmentArrayOfButtons addObject:@"Hidden Gems"];
//    [_segmentArrayOfButtons addObject:@"Top places to eats"];

    NSArray *segmetItemsArray = [NSArray arrayWithObjects:@"Local Highlights",@"Hidden Gems",@"Top places to eats", nil];
    
    segmentTag = 0;
   // [self drawSegmentController:segmetItemsArray];
   
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [self apiLocalHighlights:_strForCity];
//    });
   // [self performSelector:@selector(apiLocalHighlights) withObject:_strForCity afterDelay:0.1 ];
   // [self apiLocalHighlights:_strForCity];
  
   // [self apiCalltopPlacesEat:_strForCity];

    _letArray = [[NSMutableArray alloc]init ];
    _longArray = [[NSMutableArray alloc]init ];

    [self setupLeftMenuButton];
  

}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    CLLocation *location = [locations lastObject];
    
    [locationManager stopUpdatingLocation];
    locationManager.delegate = nil;

    
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    lattitude = [NSString stringWithFormat:@"%.5f",location.coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%.5f",location.coordinate.longitude];
    
    static dispatch_once_t onceToken = 0;

    dispatch_once(&onceToken, ^{
               
        
        [self apiLocalHighlights:lattitude :longitude];
        
        
            });
    
}

-(void)drawSegmentController:(NSArray *)dataArray {

    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    for(UIView *subview in [scrollView subviews]) {
        [subview removeFromSuperview];
    }
    [_segmentArrayOfButtons removeAllObjects];
    
    scrollView = [[UIScrollView alloc] init];
    //scrollView.backgroundColor = [UIColor grayColor ];
    scrollView.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1];
    
    CGFloat height =  [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationController.navigationBar.bounds.size.height; ;
    
    scrollView.frame = CGRectMake(0, height, screenWidth , 55);
    
    CGFloat finalWidth = 10;
    
    for (int i = 0; i <dataArray.count ; i++) {
        
        CGSize stringsize = [ [dataArray objectAtIndex:i] sizeWithFont:[UIFont systemFontOfSize:17]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        button.tag = i;
        button.titleLabel.font = [UIFont systemFontOfSize:17];
        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        
        if (i==0) {
            
            [button setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
             
        }

        [button addTarget:self
                   action:@selector(buttonClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:[dataArray objectAtIndex:i] forState:UIControlStateNormal];
       
        [button setFrame:CGRectMake(finalWidth,05,stringsize.width, 45)];
         [scrollView addSubview:button];
        [_segmentArrayOfButtons addObject:button ];
        finalWidth = finalWidth + button.frame.size.width+ 25;
    }
    

    scrollView.showsHorizontalScrollIndicator = NO;

    scrollView.contentSize = CGSizeMake(finalWidth, 55);
    
    [[[[UIApplication sharedApplication] delegate]window] addSubview:scrollView];

    
}


-(void) buttonClicked:(UIButton*)sender
{
    NSLog(@"you clicked on button %d", sender.tag);
    
    
    for (int i = 0; i<_segmentArrayOfButtons.count; i++) {
        
        if (i == sender.tag) {
             [[_segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        }
        else {
           [ [_segmentArrayOfButtons objectAtIndex:i] setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        

    }
    
    
    if (sender.tag==0) {
      
        segmentTag = 0 ;
       // [self apiLocalHighlights:_strForCity];
    }
    
    else if (sender.tag==1) {
        segmentTag = 1 ;
        [self apiCallHiddenGems:_strForCity];
    }else if (sender.tag==2) {
        
        segmentTag = 2 ;
        [self apiCalltopPlacesEat:_strForCity];
       // [self apiCalltopPlacesEat:strCityId];
    }
    
  //  UIView *view; //View to center
   // UIScrollView *scrollView; //scroll view
    
    CGPoint point = CGPointMake(sender.frame.origin.x + sender.frame.size.width / 2,
                                sender.frame.origin.y + sender.frame.size.height / 2);
    
    CGRect rectToZoom = CGRectMake(point.x * scrollView.zoomScale - roundf(scrollView.frame.size.width /2.),
                                   point.y * scrollView.zoomScale - roundf(scrollView.frame.size.height /2.),
                                   scrollView.frame.size.width,
                                   scrollView.frame.size.height);
    
    [scrollView scrollRectToVisible:rectToZoom animated:YES];
    
 
     // [self loadDataAccordingButtonSelect:completeDataArray indexOfbuttonSelect:sender.tag];
}


-(void)apiLocalHighlights:(NSString *)latt :(NSString *)longi{

    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeBlack];

__block NSMutableDictionary *resultsDictionary;

// NSString *latt = [NSString stringWithFormat:@"%.5f",[[[_cityDic valueForKey:@"coordinates"]valueForKey:@"latitude"]doubleValue]];
//    NSString *longi = [NSString stringWithFormat:@"%.5f",[[[_cityDic valueForKey:@"coordinates"]valueForKey:@"longitude"]doubleValue]];
    
NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 latt,@"Latitude",longi,@"Longitude",nil];//if your json structure is something like {"title":"first title","blog_id":"1"}
if ([NSJSONSerialization isValidJSONObject:userDictionary]) {//validate it
NSError* error;
NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userDictionary options:NSJSONWritingPrettyPrinted error: &error];
NSString *urlstring = @"https://testtravelapi.azurewebsites.net/api/bifrost/LocalHighlights";
    
NSURL* url = [NSURL URLWithString:urlstring];
NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
[request setHTTPMethod:@"POST"];//use POST
[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
[request setValue:[NSString stringWithFormat:@"%d",[jsonData length]] forHTTPHeaderField:@"Content-length"];
[request setHTTPBody:jsonData];//set data
 __block NSError *error1 = [[NSError alloc] init];

 //use async way to connect network
[NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
{
    if ([data length]>0 && error == nil) {
        resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
        NSLog(@"resultsDictionary is %@",resultsDictionary);
       // cityarr = [resultsDictionary valueForKey:@"results"];
      
        [SVProgressHUD dismiss];
       
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self drawMarkers:[[[resultsDictionary valueForKey:@"results"]lastObject]valueForKey:@"pois"]];
       
        });
        
                
        

    } else if ([data length]==0 && error ==nil) {
        NSLog(@" download data is null");
        [SVProgressHUD dismiss];
    } else if( error!=nil) {
        [SVProgressHUD dismiss];
        NSLog(@" error is %@",error);
    }
}];
    
}
    
}


-(void)apiCalltopPlacesEat:(NSString *)citystr{

    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeBlack];

__block NSMutableDictionary *resultsDictionary;

NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"", @"", nil];//if your json structure is something like {"title":"first title","blog_id":"1"}
//if ([NSJSONSerialization isValidJSONObject:userDictionary]) {//validate it
NSError* error;
NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userDictionary options:NSJSONWritingPrettyPrinted error: &error];
NSString *urlstring = [NSString stringWithFormat:@"https://testtravelapi.azurewebsites.net/api/bifrost/POItoEats?cityName=%@",citystr];
    
NSURL* url = [NSURL URLWithString:urlstring];
NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
[request setHTTPMethod:@"POST"];//use POST
[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
[request setValue:[NSString stringWithFormat:@"%d",[jsonData length]] forHTTPHeaderField:@"Content-length"];
[request setHTTPBody:jsonData];//set data
 __block NSError *error1 = [[NSError alloc] init];

 //use async way to connect network
[NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
{
    if ([data length]>0 && error == nil) {
        resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
        NSLog(@"resultsDictionary is %@",resultsDictionary);
       // cityarr = [resultsDictionary valueForKey:@"results"];
      
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self drawMarkers:[resultsDictionary valueForKey:@"results"]];
       
        });
        
        
      
        
      
       
      
                
        

    } else if ([data length]==0 && error ==nil) {
        NSLog(@" download data is null");
        [SVProgressHUD dismiss];
    } else if( error!=nil) {
        [SVProgressHUD dismiss];
        NSLog(@" error is %@",error);
    }
}];
    
}


-(void)apiCallHiddenGems: (NSString *)citystr{

    
    [SVProgressHUD showWithStatus:@"Loading" maskType:SVProgressHUDMaskTypeBlack];

__block NSMutableDictionary *resultsDictionary;

NSDictionary *userDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"", @"", nil];//if your json structure is something like {"title":"first title","blog_id":"1"}
//if ([NSJSONSerialization isValidJSONObject:userDictionary]) {//validate it
NSError* error;
NSData* jsonData = [NSJSONSerialization dataWithJSONObject:userDictionary options:NSJSONWritingPrettyPrinted error: &error];
NSString *urlstring = [NSString stringWithFormat:@"https://testtravelapi.azurewebsites.net/api/bifrost/POIhiddenGems?cityName=%@",citystr];
    
NSURL* url = [NSURL URLWithString:urlstring];
NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0];
[request setHTTPMethod:@"POST"];//use POST
[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
[request setValue:[NSString stringWithFormat:@"%d",[jsonData length]] forHTTPHeaderField:@"Content-length"];
[request setHTTPBody:jsonData];//set data
 __block NSError *error1 = [[NSError alloc] init];

 //use async way to connect network
[NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error)
{
    if ([data length]>0 && error == nil) {
        resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error1];
        NSLog(@"resultsDictionary is %@",resultsDictionary);
       // cityarr = [resultsDictionary valueForKey:@"results"];
      
        [SVProgressHUD dismiss];
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self drawMarkers:[resultsDictionary valueForKey:@"results"]];
       
        });
        
        
      
        
      
       
      
                
        

    } else if ([data length]==0 && error ==nil) {
        NSLog(@" download data is null");
        [SVProgressHUD dismiss];
    } else if( error!=nil) {
        [SVProgressHUD dismiss];
        NSLog(@" error is %@",error);
    }
}];
    
}


-(NSString *)citytext:(int )index forstring :(NSArray *)completeArray {
    
    NSString *finalString;
    
    NSString *completeStr = [[completeArray objectAtIndex:index] valueForKey:@"notes"];
    
    if([completeStr containsString:@"</strong>"]) {
    
    NSString *listItems = [[completeStr componentsSeparatedByString:@"</strong>"] firstObject];
     finalString = [[listItems componentsSeparatedByString:@"<strong>"] lastObject];
    
        NSString *String = [[listItems componentsSeparatedByString:@"<strong>"] lastObject];
        
            finalString = [String stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
        
    }
    
    else {
        
        finalString = nil;
    }

    return finalString;
}





-(void)setupLeftMenuButton{
    //for Back in left side
    UIImage *backImg = [[UIImage imageNamed:@"arrow-left-white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] ;
    UIButton *backButton  = [[UIButton alloc] initWithFrame:CGRectMake(0,6, 07, 07)];
    [backButton addTarget:self action:@selector(backButtonTapp) forControlEvents:UIControlEventTouchUpInside];
    [backButton setBackgroundImage:backImg forState:UIControlStateNormal];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc]
                                   initWithTitle:nil
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(flipVie)];
    
    textButton.tintColor = [UIColor whiteColor];
    
    [textButton setImage:[UIImage imageNamed:@"live_chat.png"]];
    
    self.navigationItem.rightBarButtonItems = @[ textButton];
    
    UIBarButtonItem *textButton1 = [[UIBarButtonItem alloc]
                                   initWithTitle:@"WHAT'S NEAR ME"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                    action:@selector(backButtonTapp)];
    
    textButton1.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItems = @[searchItem, textButton1];;
}



-(void)backButtonTapp {
    
    [scrollView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}




-(CLLocationCoordinate2D)findCenterPoint:(CLLocationCoordinate2D)_lo1 :(CLLocationCoordinate2D)_loc2 {
    CLLocationCoordinate2D center;

    double lon1 = _lo1.longitude * M_PI / 180;
    double lon2 = _loc2.longitude * M_PI / 180;

    double lat1 = _lo1.latitude * M_PI / 180;
    double lat2 = _loc2.latitude * M_PI / 180;

    double dLon = lon2 - lon1;

    double x = cos(lat2) * cos(dLon);
    double y = cos(lat2) * sin(dLon);

    double lat3 = atan2( sin(lat1) + sin(lat2), sqrt((cos(lat1) + x) * (cos(lat1) + x) + y * y) );
    double lon3 = lon1 + atan2(y, cos(lat1) + x);

    center.latitude  = lat3 * 180 / M_PI;
    center.longitude = lon3 * 180 / M_PI;

    return center;
}














-(void)ZoominOutMapDays:(CGFloat)level :(NSArray *)lettArray :(NSArray *)longArray
{
   
    int median ;
    if (lettArray.count%2==0) {
        median= lettArray.count/2;
    }
    else {
        median= lettArray.count/2 +1;
    }
    
    

    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[lettArray objectAtIndex:median] doubleValue]
                longitude:[[lettArray objectAtIndex:median] doubleValue] zoom:level];
    [_mapView animateToCameraPosition:camera];
   
      self.view = _mapView;
      
}


-(void)ZoominOutMap:(CGFloat)level
{
    NSArray *completePinArray = [_wetuContentIdDic valueForKey:@"pins"];
    int median ;
    if (completePinArray.count%2==0) {
        median= completePinArray.count/2;
    }
    else {
        median= completePinArray.count/2 +1;
    }
    
    

    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[[[completePinArray objectAtIndex:0] valueForKey:@"position"]valueForKey:@"latitude"] doubleValue]
                longitude:[[[[completePinArray objectAtIndex:0 ] valueForKey:@"position"]valueForKey:@"longitude"] doubleValue] zoom:level];
    [_mapView animateToCameraPosition:camera];
   
      self.view = _mapView;
      
}


-(void)zoomInMapView:(id)sender
{
 currentZoom = currentZoom + 1;

 [self ZoominOutMap:currentZoom];
}

-(void) zoomOutMapView:(id)sender
{
 currentZoom = currentZoom - 1;

 [self ZoominOutMap:currentZoom];
}
-(void)drawMarkers:(NSArray*)pinArray {

    [_mapView clear];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    
    int arraycount = pinArray.count;
    
        int median ;
        if (pinArray.count%2==0) {
            median= pinArray.count/2;
        }
        else {
            median= pinArray.count/2 +1;
    
        }
    
    
    
    
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[[[pinArray objectAtIndex:median]valueForKey:@"coordinates"]valueForKey:@"latitude" ] doubleValue]
                                                                longitude:[[[[pinArray objectAtIndex:median]valueForKey:@"coordinates"]valueForKey:@"longitude"] doubleValue]
                                                                                      zoom:12
                                                                                      bearing:0
                                                                                      viewingAngle:45];
       // _mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
   // [self.view addSubview:_mapView];
     
       
      _mapView = [[GMSMapView alloc] initWithFrame: CGRectMake(0, scrollView.frame.origin.y+scrollView.frame.size.height, screenWidth, screenHeight) camera:camera];
       self.view = _mapView;
      _mapView.delegate = self;
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];

    for (int i =0; i<pinArray.count; i++) {
        

        NSString *let = [[[pinArray objectAtIndex:i]valueForKey:@"coordinates"]valueForKey:@"latitude" ];
        NSString *longi = [[[pinArray objectAtIndex:i]valueForKey:@"coordinates"]valueForKey:@"longitude" ];

            
            CLLocationCoordinate2D position1 = CLLocationCoordinate2DMake([let doubleValue], [longi doubleValue]);
                    GMSMarker *marker = [[GMSMarker alloc] init];
                       marker = [GMSMarker markerWithPosition:position1];
                      bounds = [bounds includingCoordinate:marker.position];
                       marker.title = [[pinArray objectAtIndex:i]valueForKey:@"name"];
                       marker.appearAnimation = kGMSMarkerAnimationPop;
                      
                       UIImage *markerImg = [UIImage imageNamed:@"pin1.png"];
                          // marker.snippet= [dataDic valueForKey:@"category"];
                       marker.snippet = [NSString stringWithFormat:@"Rating: %.1f out of 10",[[[pinArray objectAtIndex:i] valueForKey:@"score"]floatValue]];
                     marker.userData = [pinArray objectAtIndex:i];
                        marker.icon = markerImg;
                        marker.map = _mapView;

                
            
    }
    
    if (segmentTag ==1) {
        
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100.0f]];
    }else {
    
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:60.0f]];
        
    }

}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
   
    NSLog(@"%@",marker.userData);
    
   // [ValidationAndJsonVC displayAlert:@"Working" HeaderMsg:@"Work on progress!"];
    
}

-(UIView *)mapView:(GMSMapView *) aMapView markerInfoWindow:(GMSMarker*) marker
{
    NSDictionary *dic = marker.userData;
    UIView *InfoWindow = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 220, 150)];
        InfoWindow.layer.borderColor = [UIColor lightGrayColor].CGColor;
        InfoWindow.layer.cornerRadius = 8.0f;
        InfoWindow.layer.masksToBounds = true;
//          InfoWindow.layer.borderWidth = 1.0f;
        InfoWindow.backgroundColor = [UIColor whiteColor];
    
    NSArray *imageArray = [dic valueForKey:@"images"];
        marker.tracksInfoWindowChanges = true;

    if ([imageArray count] >0) {
    UIScrollView *horizontalScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 220, 100)];
        [InfoWindow addSubview:horizontalScroll];
        
        NSString *imageStr = [[[[[dic valueForKey:@"images"] objectAtIndex:0]valueForKey:@"sizes" ] valueForKey:@"medium"] valueForKey:@"url"];
        marker.tracksInfoWindowChanges = true;
        UIImageView *imageview = [[UIImageView alloc]
                                  initWithFrame:CGRectMake(0, 0, 220, 100)];
        imageview.layer.cornerRadius = 8.0f;
        //imageview.layer.masksToBounds = true;
              [InfoWindow addSubview:imageview];
        imageview.image = [UIImage imageNamed:@"default2.png"];
           // imageview.backgroundColor = [UIColor blackColor];

                dispatch_async(dispatch_get_main_queue(), ^{

    [self downloadImageWithURL:[NSURL URLWithString: imageStr] completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded) {

                    imageview.image = image;
                }
                  }];

                });


    }else {

        UIImageView *imageview = [[UIImageView alloc]
                                  initWithFrame:CGRectMake(0, 0, 220, 100)];
        [InfoWindow addSubview:imageview];
        imageview.layer.cornerRadius = 8.0f;
        imageview.layer.masksToBounds = true;
            imageview.backgroundColor = [UIColor blackColor];
        imageview.image = [UIImage imageNamed:@"default2.png"];


    }
        
               UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 102, 220, 20)];
                nameLabel.text = [dic valueForKey:@"name"];
                nameLabel.font =[UIFont fontWithName:@"Lato-Bold" size:17.0f];
               nameLabel.textColor = [UIColor blackColor];
                [InfoWindow addSubview:nameLabel];
    
    
    
    UILabel *ratinglabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 122, 220, 20)];
    ratinglabel.font = [UIFont fontWithName:@"Lato" size:14.0f];
    ratinglabel.textColor = [UIColor orangeColor];
    ratinglabel.textAlignment = NSTextAlignmentLeft;
    
    NSString *stringRating = [NSString stringWithFormat:@"Rating: %.1f out of 10",[[dic valueForKey:@"score"]floatValue]];
    ratinglabel.text = stringRating;
    [InfoWindow addSubview:ratinglabel];
    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button addTarget:self
//               action:@selector(buttonMethod:)
//     forControlEvents:UIControlEventTouchUpInside];
//  //  button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//   // button.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:179.0/255.0 blue:224.0/255.0 alpha:1];
//    button.backgroundColor = [UIColor blackColor];
//
//    //button.tag = i;
//   // fbbutton.titleLabel.textColor = [UIColor blueColor];
//    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    button.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:16.0f];
//    button.layer.cornerRadius = 12.0f;
//    button.layer.masksToBounds = true;
//
//    [button setTitle:@"Directions" forState:UIControlStateNormal];
//    button.frame = CGRectMake(10, 145, 100.0, 28);
//    [InfoWindow addSubview:button];
    
    
    
    //customize the UIView, for example, in your case, add a UILabel as the subview of the view
    return InfoWindow;
}


-(void)buttonMethod :(UIButton*)sender {
    int index= sender.tag;
   
    
}
    
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
    {
        
        if (url!= nil ){
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   if ( !error )
                                   {
                                       UIImage *image = [[UIImage alloc] initWithData:data];
                                       completionBlock(YES,image);
                                   } else{
                                       completionBlock(NO,nil);
                                   }
                               }];
      }
    }

@end
